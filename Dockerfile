FROM ubuntu:18.04
MAINTAINER Tim Isbister <tim.isbister@foi.se>

ENV PYTHONUNBUFFERED=0

RUN apt-get upgrade -y && apt-get dist-upgrade
RUN apt-get update && apt-get install -y \
    build-essential \
    python3-pip \
&& rm -rf /var/lib/apt/lists/*

COPY . .

RUN pip3 install --upgrade pip
RUN pip3 install -r requirements.txt
RUN python3 -m nltk.downloader punkt
RUN python3 -m nltk.downloader stopwords

EXPOSE 1338

CMD ["python3", "app.py"]