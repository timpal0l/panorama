import base64
from datetime import datetime

import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
from dash.dependencies import Input, Output
from pymongo import MongoClient

from plots import time_series, post_counts, user_counts, hate_monitor_timeseries

mongo_object = MongoClient('spindel.foi.se', 27017, username='dsgOwner', password='DSG2019mongo!')

app = dash.Dash(__name__)

wordlists = {}
# dataframe_ts = None
app.config['suppress_callback_exceptions'] = True

current_year = datetime.now().strftime("%Y")
marks = {}
for year in range(int(current_year) - 8, int(current_year) + 1):
    marks[year] = str(year)


def list_dbs():
    databases = mongo_object.list_database_names()
    try:
        databases.remove('admin'), databases.remove('local'), databases.remove('config')
    except ValueError:
        print('Database not found, not removing')

    _databases = []

    for database in databases:
        _databases.append({'label': database, 'value': database})
    return _databases


databases_list = list_dbs()
filter_list = [{'label': 'Time Series (days)', 'value': 'time_series_days'},
               {'label': 'Time Series (months)', 'value': 'time_series_months'},
               {'label': 'Time Series (years)', 'value': 'time_series_years'},
               {'label': 'Time Series (normalized) (days)', 'value': 'time_series_norm_days'},
               {'label': 'Time Series (normalized) (months)', 'value': 'time_series_norm_months'},
               {'label': 'Time Series (normalized) (years)', 'value': 'time_series_norm_years'},
               {'label': 'Post Counts (days)', 'value': 'post_counts_days'},
               {'label': 'Post Counts (months)', 'value': 'post_counts_months'},
               {'label': 'Post Counts (years)', 'value': 'post_counts_years'},
               {'label': 'Active Users (days)', 'value': 'user_counts_days'},
               {'label': 'Active Users (months)', 'value': 'user_counts_months'},
               {'label': 'Active Users (years)', 'value': 'user_counts_years'},
               {'label': 'Hate Monitor (days)', 'value': 'hate_monitor_days'},
               {'label': 'Hate Monitor (months)', 'value': 'hate_monitor_months'},
               {'label': 'Hate Monitor (years)', 'value': 'hate_monitor_years'}]
app.layout = \
    html.Div(
        [
            html.H1('Panorama'),
            html.Div(
                [
                    html.Div([
                        html.H3('Data'),
                        dcc.Dropdown(id='dbs_list', options=databases_list, multi=True), html.P(),
                        dcc.Checklist(id='collection',
                                      options=[
                                          {'label': 'Comments', 'value': 'comments'},
                                          {'label': 'Articles', 'value': 'articles'}
                                      ],
                                      values=['comments', 'articles'],
                                      labelStyle={'display': 'inline-block'}
                                      )]),
                    html.Div([
                        html.H3('Interval'),
                        dcc.RangeSlider(id='date_slider',
                                        min=int(current_year) - 9,
                                        max=int(current_year) + 2,
                                        step=None,
                                        value=[int(current_year) - 1, int(current_year)],
                                        marks=marks
                                        ), ], className=''),

                    html.Div([html.H3('Filter'), dcc.Dropdown(id='filter_list', options=filter_list, multi=False)]),
                    html.P(),
                    html.Div(id='upload-data-placeholder'),
                    html.Div(id='output-data-upload', style={'display': 'none'}),
                    html.Div(id='graphs-placeholder')],

                className='container'), ],
        className='header')


@app.callback(Output('upload-data-placeholder', 'children'),
              [Input('filter_list', 'value')])
def display_upload_area(value):
    if value:
        if 'time_series' in value or 'monitor' in value:
            return html.Div([dcc.Upload([
                'Drag and Drop or ',
                html.A('Select wordlists')
            ],
                style={
                    'lineHeight': '60px',
                    'borderWidth': '1px',
                    'borderStyle': 'dashed',
                    'borderRadius': '5px',
                    'textAlign': 'center',
                    'margin': '10px',
                }, id='upload-data', multiple=True)])
        else:
            return None


@app.callback(Output('output-data-upload', 'children'),
              [Input('upload-data', 'contents'),
               Input('upload-data', 'filename')])
def update_output(list_of_contents, list_of_names):
    wordlists.clear()
    if list_of_contents is not None:
        for content, filename in zip(list_of_contents, list_of_names):
            _, stringy = content.split(',')
            tokens = base64.b64decode(stringy).decode('utf-8')
            wordlists[filename] = tokens
            # wordlist[filename] = set(tokens)
        # return key names, the values stored in global variable stuff_dic as a side effet
        return list(wordlists)
    else:
        return None


@app.callback(
    Output(component_id='graphs-placeholder', component_property='children'),
    [Input(component_id='dbs_list', component_property='value'),
     Input(component_id='date_slider', component_property='value'),
     Input(component_id='filter_list', component_property='value'),
     Input(component_id='output-data-upload', component_property='children'),
     Input(component_id='collection', component_property='values')],
)
def plot(dbs_list, date_slider, plot_type, topics, collection):
    if plot_type:
        if 'counts' in plot_type:
            topics = False

    if not dbs_list or plot_type is None or topics is None or collection is []:
        return None

    if 'time_series' in plot_type:
        graphs = []
        title = 'Term frequency'
        normalize = False

        if 'days' in plot_type:
            date_interval = 'days'
        elif 'months' in plot_type:
            date_interval = 'months'
        elif 'years' in plot_type:
            date_interval = 'years'

        if 'norm' in plot_type:
            normalize = True
            title = 'Relative term frequency'

        dataframe_ts = time_series(mongo_object, dbs_list, collection, wordlists, date_slider, normalize, date_interval)
        print(dataframe_ts)
        for e, wordlist in enumerate(wordlists):
            graph = dcc.Graph(id=str(e),
                              figure=go.Figure(data=[
                                  go.Scatter(
                                      x=dataframe_ts.ix[:, 0],
                                      y=dataframe_ts.ix[:, e + 1],
                                      opacity=0.7,
                                      mode='lines',
                                      line=dict(
                                          shape='spline'
                                      )
                                  ),
                              ], layout=go.Layout(
                                  title=list(dataframe_ts)[e + 1].split('.')[0].title(),
                                  xaxis=dict(title='Time'),
                                  yaxis=dict(title=title)
                              )
                              ),
                              )
            graphs.append(html.Div(graph, className='content-container'))
        return graphs

    elif 'post_counts' in plot_type:
        if 'days' in plot_type:
            date_interval = 'days'
        elif 'months' in plot_type:
            date_interval = 'months'
        elif 'years' in plot_type:
            date_interval = 'years'

        dataframe_pc = post_counts(mongo_object, dbs_list, collection, date_slider, date_interval)

        graph = dcc.Graph(
            figure=go.Figure(data=[
                go.Scatter(
                    x=dataframe_pc.ix[:, 0],
                    y=dataframe_pc.ix[:, 1],
                    opacity=0.7,
                    mode='lines',
                    line=dict(
                        shape='spline'
                    )
                ),
            ], layout=go.Layout(
                # title=list(dataframe_pc)[1],
                xaxis=dict(title='Time'),
                yaxis=dict(title='Post counts')
            )))

        return html.Div(graph, className='content-container')

    elif 'user_counts' in plot_type:
        if 'days' in plot_type:
            date_interval = 'days'
        elif 'months' in plot_type:
            date_interval = 'months'
        elif 'years' in plot_type:
            date_interval = 'years'

        dataframe_pc = user_counts(mongo_object, dbs_list, collection, date_slider, date_interval)

        graph = dcc.Graph(
            figure=go.Figure(data=[
                go.Scatter(
                    x=dataframe_pc.ix[:, 0],
                    y=dataframe_pc.ix[:, 1],
                    opacity=0.7,
                    mode='lines',
                    line=dict(
                        shape='spline'
                    )
                ),
            ], layout=go.Layout(
                # title=list(dataframe_pc)[1],
                xaxis=dict(title='Time'),
                yaxis=dict(title='Active Users')
            )))

        return html.Div(graph, className='content-container')

    elif 'monitor' in plot_type:
        graphs = []

        if 'days' in plot_type:
            date_interval = 'days'
        elif 'months' in plot_type:
            date_interval = 'months'
        elif 'years' in plot_type:
            date_interval = 'years'

        hate_monitor_ts = hate_monitor_timeseries(mongo_object, dbs_list, collection, wordlists, date_slider,
                                                  date_interval)

        for e, wordlist in enumerate(wordlists):
            graph = dcc.Graph(id=str(e),
                              figure=go.Figure(data=[
                                  go.Scatter(
                                      x=hate_monitor_ts.ix[:, 0],
                                      y=hate_monitor_ts.ix[:, e + 1],
                                      mode='lines',
                                      line=dict(
                                          shape='spline'
                                      ),
                                      marker=dict(
                                          color='#F8766D'
                                      )
                                  ),
                              ], layout=go.Layout(
                                  title=list(hate_monitor_ts)[e + 1].split('.')[0].title(),
                                  xaxis=dict(title='Time'),
                                  yaxis=dict(title='Hate Occurrences')
                              )
                              ),
                              )
            graphs.append(html.Div(graph, className='content-container'))
        return graphs


if __name__ == '__main__':
    app.run_server(host='0.0.0.0', port=1338, debug=False)
