from collections import Counter
from datetime import datetime

import pandas as pd
from dateutil.relativedelta import relativedelta
from nltk import word_tokenize, MWETokenizer
from sklearn.externals import joblib
from os import path

mwe_tokenizer = MWETokenizer(separator=" ")  # byggs den på hela tiden?
hate_estimator = joblib.load(path.join('models', 'hate.pkl'))


def time_series(mongo_object, databases, collections, topics, time, normalize, date_interval):
    date = datetime(time[0], 1, 1)
    stop_date = datetime(time[1], 12, 31)
    term_counter = Counter()

    dates = []
    values = []
    lexicon_terms = []

    if 'days' in date_interval:
        step_size = relativedelta(days=1)
    elif 'months' in date_interval:
        step_size = relativedelta(months=1)
    elif 'years' in date_interval:
        step_size = relativedelta(years=1)

    for topic in topics:
        lexicon_terms += filter(None, topics[topic].split('\n'))
    # aggregate all topics to the tokenizer
    lexicon_terms = set(lexicon_terms)

    for lexicon_term in lexicon_terms:
        if " " in lexicon_term:
            mwe_tokenizer.add_mwe(tuple(lexicon_term.lower().split()))

    while date <= stop_date:
        counts = dict.fromkeys(topics, 0)

        texts_cursors = []
        for database in databases:
            for collection in collections:
                texts_cursors.append(mongo_object[database][collection].find(
                    {'date': {'$gte': date, '$lt': date + step_size}}))

        aggregated_texts = []
        for text_cursor in texts_cursors:
            for text in text_cursor:
                aggregated_texts += mwe_tokenizer.tokenize(word_tokenize(text['text'].lower()))

        term_counter.update(aggregated_texts)
        # print(aggregated_texts)
        for topic in topics:
            if normalize:
                try:
                    counts[topic] += sum(
                        term_counter[term] for term in set(filter(None, topics[topic].split('\n')))) / len(
                        aggregated_texts)
                except ZeroDivisionError:
                    counts[topic] += 0
            else:
                counts[topic] += sum(term_counter[term] for term in set(filter(None, topics[topic].split('\n'))))

        values.append(counts)
        dates.append(date)
        date = date + step_size
        term_counter.clear()

    dataframe = pd.DataFrame(values, dates)
    dataframe.reset_index(inplace=True)

    return dataframe


def post_counts(mongo_object, databases, collections, time, date_interval):
    date = datetime(time[0], 1, 1)
    stop_date = datetime(time[1], 12, 31)

    dates = []
    values = []
    if 'days' in date_interval:
        step_size = relativedelta(days=1)
    elif 'months' in date_interval:
        step_size = relativedelta(months=1)
    elif 'years' in date_interval:
        step_size = relativedelta(years=1)

    while date <= stop_date:
        texts_cursors = []
        counts = 0
        for database in databases:
            for collection in collections:
                texts_cursors.append(mongo_object[database][collection].find(
                    {'date': {'$gte': date, '$lt': date + step_size}}))

        for text_cursor in texts_cursors:
            counts += text_cursor.count()

        values.append(counts)
        dates.append(date)
        date = date + step_size

    dataframe = pd.DataFrame(values, dates)
    dataframe.reset_index(inplace=True)

    return dataframe


def user_counts(mongo_object, databases, collections, time, date_interval):
    date = datetime(time[0], 1, 1)
    stop_date = datetime(time[1], 12, 31)
    dates = []
    values = []
    if 'days' in date_interval:
        step_size = relativedelta(days=1)
    elif 'months' in date_interval:
        step_size = relativedelta(months=1)
    elif 'years' in date_interval:
        step_size = relativedelta(years=1)

    while date <= stop_date:
        texts_cursors = []
        counts = 0
        for database in databases:
            for collection in collections:
                texts_cursors.append(mongo_object[database][collection].find(
                    {'date': {'$gte': date, '$lt': date + step_size}}))

        for text_cursor in texts_cursors:
            counts += len(text_cursor.distinct('user'))

        values.append(counts)
        dates.append(date)
        date = date + step_size

    dataframe = pd.DataFrame(values, dates)
    dataframe.reset_index(inplace=True)

    return dataframe


def hate_monitor_timeseries(mongo_object, databases, collections, entities_dic, time, date_interval):
    date = datetime(time[0], 1, 1)
    stop_date = datetime(time[1], 12, 31)

    dates = []
    values = []

    if 'days' in date_interval:
        step_size = relativedelta(days=1)
    elif 'months' in date_interval:
        step_size = relativedelta(months=1)
    elif 'years' in date_interval:
        step_size = relativedelta(years=1)

    while date <= stop_date:
        counts = dict.fromkeys(entities_dic, 0)

        texts_cursors = []
        for database in databases:
            for collection in collections:
                texts_cursors.append(mongo_object[database][collection].find(
                    {'date': {'$gte': date, '$lt': date + step_size}}))

        for text_cursor in texts_cursors:
            for text in text_cursor:
                raw_text = text['text'].lower()
                for entity in entities_dic:
                    aggregated_texts = []
                    entities = set(filter(None, entities_dic[entity].split('\n')))
                    for _entity in entities:
                        _entity = _entity.lower()
                        if _entity in raw_text:
                            raw_text = raw_text.replace(_entity, '$t$')
                            aggregated_texts.append(raw_text)

                    if len(aggregated_texts) > 0:
                        counts[entity] += sum(
                            [1 for prediction in hate_estimator.predict(aggregated_texts) if prediction == 'negative'])
                    else:
                        counts[entity] += 0

        values.append(counts)
        dates.append(date)
        date = date + step_size

    dataframe = pd.DataFrame(values, dates)
    dataframe.reset_index(inplace=True)

    return dataframe
